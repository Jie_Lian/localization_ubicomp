\section{INTRODUCTION}

Indoor localization has attracted wide research attention, due to its potential of facilitating a variety of applications in smart homes such as security surveillance, elderly care, crowd monitoring, fitness tracking, etc.
A report has shown that a person may spend almost $88.9\%$ of the day indoors \cite{matz2014effects}. 
Also, the market value of indoor positioning and indoor navigation is expected to exceed  $\$23.6$ billion dollars in 2023~\cite{predict_indoor}, substantiating that  there is a large demand for effective indoor localization technology.
%However, indoor-localization is still a challenging problem.
The commonly used localization systems based on Global Positioning System (GPS) are not applicable to indoor environment, due to significant signal attenuation when penetrating the wall that leads to meter-level localization error.
This is unacceptably large in indoor environment, and reducing localization error at decimeter or centimeter level is highly desirable.

In the past decade, diverse technologies have been developed for indoor  localization and tracking.
While extensive methods have been developed for localization via  camera~\cite{zhang2016litell,van2014camera}, motion sensor~\cite{langlois2017indoor}, inertial measurement unit (IMU)~\cite{yuan2014localization}, floor sensor~\cite{mirshekari2018occupant}, or light sensor~\cite{hu2018lightitude, zhu2017enabling}, they  either require a user to carry/wear sensors, or require to purchase and deploy the dedicate devices/sensors.
These methods have the drawbacks of  inconvenience for use  or causing privacy issues.
For example, the wearable solutions may need the user  to wear the device all day for continuous monitoring.
The elders, sometimes, are likely to forget to wear devices, making it unsuitable for elderly monitoring.
Also, the wearable solutions are not suitable for some scenarios such as localizing undefined people.
The camera based solution could be accurate but likely to invade the user's privacy and make users feel uncomfortable.
% makes it not suitable in private place such as bedroom.
The RFID localization \cite{ni2003landmarc,jin2006indoor,wang2013dude,wang2013rf}, has become popular recently due to its low cost and ease of use.
However, most RFID-based localization techniques are based on the assumption of knowing the tags' coordinates, which is impractical.
Besides, many RFID-based systems heavily rely on ideal propagation models of RF phase or the received signal strength indicator (RSSI), which may not be feasible.


On the other hand, the radio-frequency (RF) sensing solutions leveraging the Wi-Fi and mmWave for localization have been extensively explored, producing promising results, i.e., decimeter-level accuracy in the several meters sensing range. 
However, the Wi-Fi-based solutions~\cite{adib15:NSDI:multi, gjengset14:MobiCom:phaser, xiong2013arraytrack, vasisht16:NSDI:decimeter, xiong15:MobiCom:tonetrack, qian2018:mobisys:widar2, joshi:NSDI:2015wideo, soltanaghaei18:MobiSys:multipath, kotaru15:SIGCOMM:spotfi, wang16:mobicom:lifs} occupy the data communication channels of 2.4GHz or 5GHz which are already crowded with data traffic, inevitably impacting the nearby devices to some extent, especially for those using multiple channels so as to achieve good performance. 
In addition, most of such systems require regular maintenance, or some of them need specialized signals, hindering their wide deployment.
%In addition, the Wi-Fi-based solutions are sensitive to noise, requiring complex signal processing algorithms.
The mmWave-based solutions~\cite{vasisht18:IMWUT:duet,zhao19:DCOSS:mid,gu19:MNSS:mmsense,pegoraro20::multi} do not cause interference to home devices, but they require the specialized mmWave radar which is typically expensive. 

In contrast to the aforementioned solutions, acoustic sensing is promising for localization  which can take advantage of the ubiquitously available audio devices without competing for radio resources with other home devices.
The low sampling frequency of the audio signal enables signal processing to be implemented on smart device.
Existing efforts~\cite{lin19:MobiCom:rebooting, cheng20:INFOCOM:acouradar, mao16:MobiCom:cat,nandakumar17:IMWUT:covertband, peng2012beepbeep, zhou17:ENS:battracker,zhang2018vernier, liu2015guoguo} for human localization via acoustic sensing are device-dependent, requiring users to carry smartphones. 
%These solutions can be considered as phone-phone localization. 
Although device-free (without carrying the devices) acoustic solutions \cite{gupta12:HFCS:soundwave, nandakumar2016fingerio, mao19:MobiCom:rnn, yun17:MobiSys:strata, chen2017echotrack, wang2016device, wang20:push} have been proposed for tracking the movement of hand, finger, or mouth, for the purposes of localization, activity recognition, or authentication, their effective sensing ranges are extremely limited, no more than 1 meter. 
%Covertband~\cite{nandakumar17:IMWUT:covertband} implements the device-free localization, which can track human motions within several meters, but its performance is mediocre.




In this paper, we propose a novel device-free localization solution via acoustic sensing, named EchoSpot, that can be implemented in the commercially available off-the-shelf (COTS) audio devices to work in home environments. 
%Specifically, EchoSpot only relies on one speaker and one microphone to precisely spot a human location.
Different from the existing work, we only rely on one speaker and one microphone with the reliance of wall reflection to precisely spot a human location in the two-dimensional space.
\begin{comment}

If a localization technique can achieve good accuracy but use only most basic set of hardware, it will be applicable to more platforms and suitable for more applications.

Different from the existing work, we only rely on one speaker and one microphone.
So, our main challenge is how to localization by the limited device.
We use the wall reflection to help localization.
We have found the signal emit from speaker would reflect by wall after received by the microphone.
If we could determine the wall reflection, then we could successfully localization.
The signal bounce from the body to the wall then to the speaker is similar to that from the body then to the speaker.
So, some method could be exploited to find such similarity so that we could find the reflections for localization.
\end{comment}
%The follows is our design.
Specifically,  we program the audio device to control its built-in speaker to periodically emit inaudible FMCW (Frequency Modulated Continuous Wave) signals at 18kHz$\sim$23kHz and use the co-located microphone to receive the reflected signals from objects for analysis.
Based on the reflected signals, we generate time-of-flight (ToF) profile and aim to identify the peaks corresponding to the body reflection. 
By eliminating the influence from the device imperfection and from the environmental objects' reflection, we can identify ToF corresponding to the person reflection, allowing us to estimate the distance between the human body and the device.
To locate a person's position, we continue to develop a new solution to identify the ToF corresponding to the wall reflection and obtain the  path of human-wall-device. 
Then, we can calculate the position information of a person in the two-dimensional space. 
Considering the potential impact of the multi-path effect, we further apply the Kalman filter to correct the position information. 
In the end, EchoSpot is implemented on the commercial speaker and microphone, and deployed in the real home environments for performance evaluation.

Comparing to the existing approaches, our contributions can be summarized as follows.
%The Echospot has several advantages: 1) it is a software based system that can be easily self-installed and deployed; 2) it employed the COTS audio devices without requiring hardware level modification; and 3)  it leverages acoustic signals rather than radio frequency, thus free from competing for the radio spectrum resource for Wi-Fi devices.

%easier to deploy, no requirement for lighting, using COTS devices to send and receive acoustic signals, and the protection of user privacy.
%Compared with RF-based approaches that use Wi-Fi signals  or Radar signals, it is also more robust, since the RF signal always interfere with each other.

%The AOA estimation  performance is highly affected by the microphone array layout.
%Different smartspeaker has different lay out, that solutions may not suitable on different smartspeakers.
%Since Echospot only relyies on one microphone and one speaker,
%it performance would not be impacted by the microphone layout, thus would be more robust. 

%Our  main contributions are summarized as follows:
\begin{itemize}
	
	\item We design a novel device-free localization system EchoSpot, by leveraging only one speaker and  one microphone for precisely locating a human. 
	It is a software-based system and can be implemented on the  COTS audio devices for conducting the localization services, rendering a wider application range in the general house environment.
%	Since it only rely on COTS devices, it could be widely applied  in many sensing and mobile systems.
 	While leveraging the acoustic signals at $18kHz\sim23kHz$, it does not cause interference to the home Wi-Fi devices and is inaudible to human.

	
	\item A collection of acoustic signal processing techniques are developed, including generating the ToF profile, removing the impact from the device imperfection, environment objects, and multi-path effect.
	In addition, a new solution is also proposed to identify the reflection path from the wall. 
	 After applying these techniques,  EchoSpot can work suitably to spot a person's location in the home environment.
	
	\item We implement EchoSpot with the COTS speaker and microphone for proof-of-concept validation.
	We conduct experiments to demonstrate that EchoSpot can work effectively  both  to locate the static person and to track the moving person. 
	Experimental results exhibit that EchoSpot can achieve the median errors of  $8.5cm$ and $19.8cm$ for locating the static person and the moving person, respectively,  comparable to the reported results from the state-of-the-art.
	In addition, EchoSpot achieves the mean errors of $4.1cm, 9.2cm, 13.1cm, 17.9cm, $, $22.2cm$, respectively, at the distances of $1m, 2m, 3m, 4m$, and $5m$, for locating the static person.
	\end{itemize}





 

 



\begin{comment}


 which are inconvenient and cumbersome. Sometimes, people feel reluctant or forget to wear them.
Other solutions require to purchase the dedicate devices/sensors or require the excessive deployments. 

These solutions either require a user to carry the sensor, or require to deploy the dedicated camera, light sensor, or floor sensor, which thus have the drawbacks of  inconvenient, cumbersome,  and incurring non-negligible cost

, which can be roughly categorized into two  groups.

including ultrasound , cameras, RFID, WIFI, and mmwave. 
For RFID, motion sensor, they are all device-dependent (requiring the user to wear/carry the tags/sensors or deploying sensors) for tracking, which are inconvenient and cumbersome while sometime users may forget to carry them. 
There might be some people reluctant or forget to wear a wearable device.

%The camera-based solution required the dedicated camera, which are expense and raise privacy concerns, thus making residents be reluctant to deploy. 
%Some commercial hardware systems such as Kinect can track  peoplen, however, they require to use dedicated hardware. 
The camera-based solution Kinect \cite{shotton2011real}, LeapMotion \cite{weichert2013analysis} has been widely used.
However they are severely constrained by the lightening condition and may raise privacy concerns.
Also they all require dedicated hardwares. 

For Wi-Fi-based solutions, they either required to use custom hardware (e.g., USRP or WARP) or multiple access points (APs) to form the large antenna arrays, which are typically not available at home, to achieve the precise localization. 
Although WiFi based tracking has the longest range compare to other solutions, but falls short in  accuracy and reliability, only achieves decimeter-level accuracy due to its fast propagation speed and large wavelength. 
Some set of WIFI solutions requires to use ultra-bandwidths, for example 70MHz at 2.4GHz, which may inevitable bring radio spectrum competition to other smart home applications. The antenna array, sometimes, is also required to achieve the good performance.

For mmWave-based solutions, the specialized mmWave radar is required to emit the ultra frequency signals, which incurs non-negligible cost and not applicable for wide deployment. 
%As such,  a reliable  localization system has attracted a lot of research efforts recently.
%RFID based tracking always require the user to wear a device.


%Most of the existing acoustic device-free tracking can only support a range up to 100 cm [], which is insufficient for localization.

To overcome the disadvantages of the above solutions, recent years, the ultrasonic is considered introduced into the localization system.
Compare to above approaches, the ultrasonic based solution is attractive.
When compared to more common RF signals like Wi-Fi, acoustic localization gains strength as what it requires are mainly microphones and speakers, which are widely equipped on many smart devices.
Another advantage of the acoustic signal is that the sound speed is much lower than the speed of the RF signal, which implies potential for higher accuracy.




While there has been significant research interest in the use of acoustic sensing for localization, most of them could not be implemented on commodity devices which permit  the device-free localization. 
So, we create our system, transforms just one pair of microphones and speakers into active sonar systems to track users. 
At a high level, we transmit acoustic pulses in the 18-23 kHz range from the speaker and track reflections from the human body on the microphones. 
A set of software-based signal processing technologies will be developed to extract the location information from the reflected FMCW signal. 
First we apply the cross correlation to generate the TOF profile.
Then we eliminate the influence of the unsynchronization of the speaker and microphone by eliminate the start time error. 
Then we apply Polynomial curve fitting eliminate the influence of the reflections of the static object to get the pure reflections.
The remaining peaks on the TOF profile only contain the moving object reflections.
We locate the body reflections and we compare the body reflections with other signals to locate the wall reflection, then we get the location of wall reflection.
We could locate the people by these two reflections. 


There exist some approach, for example Covertband \cite{nandakumar2017covertband}, implement the activity recognition by leting the speaker generate the inaudible OFDM signal and analyze the reflected signal.
However it is implement on at least two microphone
\cite{mao2019rnn} implement the hand motion tracking, and shows the potential of the localization. 
However, their system still need specialized microphone array. 
Compare to their approach, our system only need one pair of speaker and microphone for localization.


In this project, we propose to utilize acoustic signals generated by commodity devices for smart home localization. 
Compare to the other approach (RF, sensor), the advantage of the acoustic-based solution, including but are not limited to:
First, The system's performance does not depend on lighting condition and it can be used in darkness. 
%Compared with RF-based approaches, acoustic signals have unique advantages.
Second,  the sampling rate of acoustic signals is low so that all processing can be done in software. We can easily customize transmission signals and process received signals in software without special hardware. 
Third, the slow propagation speed of acoustic signals makes it possible to achieve high identification accuracy.
The resolution is determined by the ratio between the signal propagation speed and bandwidth.
To achieve the same resolution of acoustic system using 10KHz bandwidth,an RF based system needs around 9GHz bandwidth. 
Forth, its a device free solution so that the user does not require to wear other device.


%The SNR from can be very low at room scale, which significantly degrades the tracking accuracy. And multipath can dominate received signals at room scale and cause severe interference.


Our contributions can be summarized as follows.
\begin{itemize}
	\item We demonstrate the feasibility that a single pair of COTS speaker and microphone could track a people's moving. 
	Most of existing work are based on sensors or several pairs of microphones.
	
	\item We design a device-free tracking system, which leverages speaker and microphone to track the people moving. 
	We design an algorithm to process the multipath noise.
	We apply Kalman Filter to track the movement.
	
	
	\item We implement the system on commodity speaker and microphone and achieves a median error of 8.1cm in 3 meters.
	
	
	
\end{itemize}
\end{comment}
\begin{comment}
Indoor localization has attracted much attention for its potential applications
 in healthcare and safety/security monitoring and detection. 
A survey shows that a person spends 88.9\% of the day indoors
\cite{matz2014effects}. 
Also, the market value of indoor location-recognition is expected to exceed a \$23.6 billion dollars in 2023\cite{predict_indoor}, which means there is a large demand for effective indoor localization technology.
The localization enables a variety of indoor applications in smart homes. 
Such as security surveillance, elderly care,  crowd monitoring, fitness tracking, and many more. 
However, indoor-localization of people has become one of the most important challenges in engineering, since commonly used localization systems based on Global Positioning System (GPS) are not available in indoor environment.
The GPS signal could be too weak when it comes to indoor localization: the blocking of walls and floors severely attenuates the signal. 
On the other hand, the precision of the Standard Positioning Service (SPS) of the GPS is order of tens of meters is far from practical for indoor applications.
Thus, a solution that can achieve centimeter level indoor localization is highly desirable.
\end{comment}

%Localization in a room is the key requirement for services in smart home and emerging applications.

%One typical application is that the automatic room temperature adjustment based on humans’ quantity in a room.
%For the elderly, continuously monitoring their locations and behaviors could help to detect some emergency cases, such as a long stay in the bathroom due to suddenly falling.
%For smart building, the detection of human and estimation of room occupancy is of %particular importance, which can be used to dynamically adjust the heating, ventilation,and air-conditioning facilities to reduce energy consumption.
%For the above approach, the localization should be performed high accuracy without human activity.


%Passive motion tracking without any device carried by or attached to a person has been an exciting area of recent interest, with important applications including security surveillance[], elderly care[]. 



%For the outdoor localization systems, Global Positioning System (GPS) has provided routes to millions of drivers for years. 

\begin{comment}
%The localization has gained increased interesting in research community by employing various technologies, such as RFID, motion sensor, light sensor, camera, Wi-Fi, and millimeter wave (mmWave). 
Diverse technologies have been developed for localization and tracking including ultrasound , cameras, RFID, WIFI, and mmwave. 
For RFID, motion sensor, they are all device-dependent (requiring the user to wear/carry the tags/sensors or deploying sensors) for tracking, which are inconvenient and cumbersome while sometime users may forget to carry them. 
There might be some people reluctant or forget to wear a wearable device.

%The camera-based solution required the dedicated camera, which are expense and raise privacy concerns, thus making residents be reluctant to deploy. 
%Some commercial hardware systems such as Kinect can track  peoplen, however, they require to use dedicated hardware. 
The camera-based solution Kinect \cite{shotton2011real}, LeapMotion \cite{weichert2013analysis} has been widely used.
However they are severely constrained by the lightening condition and may raise privacy concerns.
Also they all require dedicated hardwares. 

For Wi-Fi-based solutions, they either required to use custom hardware (e.g., USRP or WARP) or multiple access points (APs) to form the large antenna arrays, which are typically not available at home, to achieve the precise localization. 
Although WiFi based tracking has the longest range compare to other solutions, but falls short in  accuracy and reliability, only achieves decimeter-level accuracy due to its fast propagation speed and large wavelength. 
Some set of WIFI solutions requires to use ultra-bandwidths, for example 70MHz at 2.4GHz, which may inevitable bring radio spectrum competition to other smart home applications. The antenna array, sometimes, is also required to achieve the good performance.

For mmWave-based solutions, the specialized mmWave radar is required to emit the ultra frequency signals, which incurs non-negligible cost and not applicable for wide deployment. 
%As such,  a reliable  localization system has attracted a lot of research efforts recently.
%RFID based tracking always require the user to wear a device.


%Most of the existing acoustic device-free tracking can only support a range up to 100 cm [], which is insufficient for localization.

To overcome the disadvantages of the above solutions, recent years, the ultrasonic is considered introduced into the localization system.
Compare to above approaches, the ultrasonic based solution is attractive.
When compared to more common RF signals like Wi-Fi, acoustic localization gains strength as what it requires are mainly microphones and speakers, which are widely equipped on many smart devices.
Another advantage of the acoustic signal is that the sound speed is much lower than the speed of the RF signal, which implies potential for higher accuracy.




While there has been significant research interest in the use of acoustic sensing for localization, most of them could not be implemented on commodity devices which permit  the device-free localization. 
So, we create our system, transforms just one pair of microphones and speakers into active sonar systems to track users. 
At a high level, we transmit acoustic pulses in the 18-23 kHz range from the speaker and track reflections from the human body on the microphones. 
A set of software-based signal processing technologies will be developed to extract the location information from the reflected FMCW signal. 
First we apply the cross correlation to generate the TOF profile.
Then we eliminate the influence of the unsynchronization of the speaker and microphone by eliminate the start time error. 
Then we apply Polynomial curve fitting eliminate the influence of the reflections of the static object to get the pure reflections.
The remaining peaks on the TOF profile only contain the moving object reflections.
We locate the body reflections and we compare the body reflections with other signals to locate the wall reflection, then we get the location of wall reflection.
We could locate the people by these two reflections. 


There exist some approach, for example Covertband \cite{nandakumar2017covertband}, implement the activity recognition by leting the speaker generate the inaudible OFDM signal and analyze the reflected signal.
However it is implement on at least two microphone
\cite{mao2019rnn} implement the hand motion tracking, and shows the potential of the localization. 
However, their system still need specialized microphone array. 
Compare to their approach, our system only need one pair of speaker and microphone for localization.


In this project, we propose to utilize acoustic signals generated by commodity devices for smart home localization. 
Compare to the other approach (RF, sensor), the advantage of the acoustic-based solution, including but are not limited to:
First, The system's performance does not depend on lighting condition and it can be used in darkness. 
%Compared with RF-based approaches, acoustic signals have unique advantages.
Second,  the sampling rate of acoustic signals is low so that all processing can be done in software. We can easily customize transmission signals and process received signals in software without special hardware. 
Third, the slow propagation speed of acoustic signals makes it possible to achieve high identification accuracy.
The resolution is determined by the ratio between the signal propagation speed and bandwidth.
To achieve the same resolution of acoustic system using 10KHz bandwidth,an RF based system needs around 9GHz bandwidth. 
Forth, its a device free solution so that the user does not require to wear other device.


%The SNR from can be very low at room scale, which significantly degrades the tracking accuracy. And multipath can dominate received signals at room scale and cause severe interference.


Our contributions can be summarized as follows.
\begin{itemize}
	\item We demonstrate the feasibility that a single pair of COTS speaker and microphone could track a people's moving. 
	Most of existing work are based on sensors or several pairs of microphones.
	
	\item We design a device-free tracking system, which leverages speaker and microphone to track the people moving. 
	We design an algorithm to process the multipath noise.
	We apply Kalman Filter to track the movement.
	 
	
	\item We implement the system on commodity speaker and microphone and achieves a median error of 8.1cm in 3 meters.
	
	
	
\end{itemize}
\end{comment}

%We can calculate the 3 -D position using at least three speakers, which uses the propagation times between the transmitters and the receiver. However it is difficult because time synchronization between the transmitters and the receiver is required. 

%Traditional indoor localization are usually based on wearable sensors, or camera, 
%The first is not as effective because it require the user carry an object, making them a poor fit for some applications.
%In elderly care, the aged are reluctant to wear a wearable device.
%Camera-based methods are mature and widely used. 
%Although camera based are precise, they are restrict to the light in the environment.
%Most of them could not work in the environment that there are no light.
%However, since they are relying on vision-based data which usually raise privacy fears and concerns.
%Also, in order to achieve high accuracy, camera-based systems require users to face the sensor in good lighting environments, which is likely to leak users’ privacy.

\begin{comment}
So, a novel idea is to use wireless sensing(e.g.Wi-Fi, Radar) for device-free localization. 
This is based on the observation that the human body can reflect wireless signals, and the reflect signals contain unique information of the human which could be used for localization.
This observation is exactly the basis of our work.
Among all the wireless sensing technologies employed for indoor localization, Wi-Fi is considered one of the most promising schemes due to its ubiquity. 
Wi-Fi is widely used to connect a wide range of devices, such as mobiles, laptops and loudspeakers in modern offices and homes. 
This provides us a large number of Wi-Fi links around us, which made the device-free localization based on wifi possible. 
Existing WiFi-based approaches typically rely on distance, or angle as the major metric for indoor localization. 
WiFi based tracking has the longest range, but only achieves decimeter-level accuracy [] due to its fast propagation speed and large wavelength. 
And falls short in  accuracy and reliability. 
But these are easily influenced by the multi-path effect.
It is possible to estimate distance using signal strength(RSSI), but RSSI performs poorly indoors because of multipath reflections. 
Existing angle-of-arrival(AoA) estimation algorithms also have similar shortcomings. 
Meanwhile, the CSI sampling frequency of commercial Wi-Fi devices is only a few hundred Hz, which restrict the resolution.
If they want to increase the localization accuracy they must increase the quantity of the transmitter and receiver which increase the cost of the system or increase the bandwidth which will influence the normal communication.
Some work utilize modified wifi device generate RF signal \cite{adib2015multi}, but these are hard to apply to commercial devices.
The radar solution are always based on the expensive mmwave radar sensor, which would cost hundred of dollars.
Moreover, such kind of special devices is difficult to utilize by ordinary people.
Audio based tracking has higher accuracy but limited range. 
Most of the existing acoustic device-free tracking can only support a range up to 100 cm [], which is insufficient for localization.
In [] implement room-scale localization, however...
Covertband implemented an acoustic localization system, but they did not concern about the walking speed of the people.
In their experiment, the participants walking very slowly to eliminate the error of the walking speed.
But in practical, the human walking speed are always around 2m/s which will cause error in their system and restrict the utilization of their work.
\end{comment}

%it relieving the competition for radio spectrum resource in smart homes.
%Second, it utilize the existing devices which does not incurring extra cost.

%\vspace{-1.11em}


